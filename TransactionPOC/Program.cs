﻿using System;
using MongoDB.Driver;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver.Core;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Collections.Generic;

namespace Transaction__POC
{
    class Program
    {
        static void Main(string[] args)
        {

            MainAsync().Wait();

        }
        static async Task MainAsync()
        {

            IMongoClient client = new MongoClient("mongodb://localhost:27017");
            IClientSessionHandle session = client.StartSession();
            UpdateEmployeeInfo(client, session);
        }
        public static void CommitWithRetry(IClientSessionHandle session)
        {
            while (true)
            {
                try
                {
                    session.CommitTransaction(); // uses write concern set at transaction start
                    Console.WriteLine("Transaction committed.");
                    break;
                }
                catch (MongoException exception)
                {
                    // can retry commit
                    if (exception.HasErrorLabel("UnknownTransactionCommitResult"))
                    {
                        Console.WriteLine("UnknownTransactionCommitResult, retrying commit operation.");
                        continue;
                    }
                    else
                    {
                        Console.WriteLine($"Excpetion during commit: {exception.Message}.");
                        throw;
                    }
                }
            }
        }
        // updates two collections in a transaction
        public static void UpdateEmployeeInfo(IMongoClient client, IClientSessionHandle session)
        {
            var employeesCollection = client.GetDatabase("hr").GetCollection<BsonDocument>("employees");
            var eventsCollection = client.GetDatabase("reporting").GetCollection<BsonDocument>("events");
            //var testCollection = client.GetDatabase("newdb").GetCollection<BsonDocument>("test");

            // select and print record before transaction
            employeesCollection.Find(new BsonDocument())
           .ForEachAsync(x => Console.WriteLine(x));

            eventsCollection.Find(new BsonDocument())
           .ForEachAsync(x => Console.WriteLine(x));
            
            //start transaction
            session.StartTransaction(new TransactionOptions(
                readConcern: ReadConcern.Snapshot,
                writeConcern: WriteConcern.WMajority));

            try
            {
                //transaction on employeescollection
                employeesCollection.UpdateOne(
                    session,
                    Builders<BsonDocument>.Filter.Eq("employee", 2),
                    Builders<BsonDocument>.Update.Set("status", "Active"));

                employeesCollection.InsertOne(
                    session,
                       new BsonDocument
                    {
                        { "employee", 3 }, 
                        {"firstname", "Roman"}, 
                        {"lastname", "John"}, 
                        {"status", "Inactive"}

                    });

                employeesCollection.DeleteOne(
                                    session,
                                    Builders<BsonDocument>.Filter.Eq("employee", 1));


                    //transaction on eventscollection
                eventsCollection.InsertOne(
                    session,
                    new BsonDocument
                    {
                        {"employee", 4 },
                        {"eventname", "event4"},
                        {"level", 4},
                        {"ratings",4},
                        {"status","Active"}
                    });

                eventsCollection.UpdateOne(
                    session,
                    Builders<BsonDocument>.Filter.Eq("employee", 2),
                    Builders<BsonDocument>.Update.Set("status", "Inactive"));

                //transaction on testcollection
                
                // testCollection.InsertOne(
                //     session,
                //     new BsonDocument
                //     {
                //         {"employee", 4 },
                //         {"eventname", "event4"},
                //         {"level", 4},
                //         {"ratings",4},
                //         {"status","Active"}
                //     });



                // select and print record after transaction
                employeesCollection.Find(new BsonDocument())
                             .ForEachAsync(x => Console.WriteLine(x));
                eventsCollection.Find(new BsonDocument())
                .ForEachAsync(x => Console.WriteLine(x));

            }
            catch (Exception exception)
            {
                Console.WriteLine($"Caught exception during transaction, aborting: {exception.Message}.");
                session.AbortTransaction();
                throw;
            }

            CommitWithRetry(session);
        }

        // private static IEnumerable<BsonDocument> CreateNewEmployees()
        // {
        //     var employee1 = new BsonDocument
        //                 {
        //                     {"employee", 1},
        //                     {"firstname", "Ugo"},
        //                     {"lastname", "Damian"},
        //                     {"designation", "SE"},
        //                     {"age", 23},
        //                     {"status", "Active"}
        //                 };

        //     var employee2 = new BsonDocument
        //                 {
        //                      {"employee", 2},
        //                     {"firstname", "Julie"},
        //                     {"lastname", "Lerman"},
        //                     {"designation", "SSE"},
        //                     {"age", 26},
        //                     {"status", "Active"}

        //                 };


        //     var newEmployees = new List<BsonDocument>();
        //     newEmployees.Add(employee1);
        //     newEmployees.Add(employee2);

        //     return newEmployees;
        // }
        // private static IEnumerable<BsonDocument> CreateNewEvents()
        // {
        //     var event1 = new BsonDocument
        //                 {
        //                     {"employee", 1},
        //                     {"eventname", "Appreciation"},
        //                     {"level", 1},
        //                     {"ratings", 4}
        //                 };

        //     var event2 = new BsonDocument
        //                 {
        //                     {"employee", 2},
        //                     {"firstname", "AwardDistribution"},
        //                     {"level", 2},
        //                     {"ratings", 5}
        //                 };


        //     var newEvents = new List<BsonDocument>();
        //     newEvents.Add(event1);
        //     newEvents.Add(event2);

        //     return newEvents;
        // }


    }
}
